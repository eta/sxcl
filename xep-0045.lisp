;;;; XEP-0045: Multi-User Chat

(in-package :sxcl)

(defun send-muc-presence (comp to muc-jid user-nickname affiliation role &key stati user-jid)
  (with-presence (comp to :from (concatenate 'string muc-jid "/" user-nickname))
    (cxml:with-element "x"
      (cxml:attribute "xmlns" +muc-user-ns+)
      (cxml:with-element "item"
        (when user-jid
          (cxml:attribute "jid" user-jid))
        (cxml:attribute "affiliation" affiliation)
        (cxml:attribute "role" role))
      (loop
        for status in stati
        do (cxml:with-element "status"
             (cxml:attribute "code" status))))))
