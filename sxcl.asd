(defsystem "sxcl"
  :depends-on ("bordeaux-threads" "event-emitter" "blackbird" "cxml" "sha1" "uuid" "babel" "qbase64" "usocket")
  :serial t
  :components
  ((:file "packages")
   (:file "utils")
   (:file "namespaces")
   (:file "component")
   (:file "xmpp")
   (:file "xep-0030")
   (:file "xep-0045")
   (:file "xep-0363")
   (:file "xep-0115")))
